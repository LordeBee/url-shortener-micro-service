package com.stanbic.urlservice.utils;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author goDigi
 */
public class LoggingUtil {

  
    private static Logger logger = LogManager.getLogger(LoggingUtil.class); 
    
    public static void logDebug(String debugInfo){
        logger.debug(debugInfo);
    }

	public static void logDebug(String debugInfo, Logger loggerIntsance){
		logger = loggerIntsance;
        logger.debug(debugInfo);
	}
       
	public static void logInfo(String info) {
		logger.info(info);
	}

	public static void logInfo(String info, Logger loggerIntsance){
		logger = loggerIntsance;
		logger.info(info);
	}
           

    public static void logError(Exception ex){
    	String message=ex.getLocalizedMessage();
		String trace =Arrays.toString(ex.getStackTrace()).replace(",", "\n");	
    	StringBuilder stacktrace= new StringBuilder(); 
    	stacktrace.append(message);
    	stacktrace.append(trace);
    	logger.error(ex);
    	logger.error(stacktrace);
	}
    
	public static void logError(Exception ex, Logger loggerIntsance){
		logger = loggerIntsance;	
		String message=ex.getLocalizedMessage();
		String trace =Arrays.toString(ex.getStackTrace()).replace(",", "\n");	
    	StringBuilder stacktrace= new StringBuilder(); 
    	stacktrace.append(message);
    	stacktrace.append(trace);
		logger.error(ex);
		logger.error(stacktrace.toString());
	}
	
	   public static void logThrowable(Throwable t){ 
		   String message=t.getLocalizedMessage();
			String trace =Arrays.toString(t.getStackTrace()).replace(",", "\n");	
	    	StringBuilder stacktrace= new StringBuilder(); 
	    	stacktrace.append(message);
	    	stacktrace.append(trace);
			logger.error(t);
			logger.error(stacktrace.toString());
		}
	    
		public static void logThrowable(Throwable t, Logger loggerIntsance){
			logger = loggerIntsance; 
			 String message=t.getLocalizedMessage();
				String trace =Arrays.toString(t.getStackTrace()).replace(",", "\n");	
		    	StringBuilder stacktrace= new StringBuilder(); 
		    	stacktrace.append(message);
		    	stacktrace.append(trace);
			logger.error(t);
			logger.error(stacktrace.toString());
		}
}
