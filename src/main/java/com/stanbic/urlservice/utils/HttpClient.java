package com.stanbic.urlservice.utils;

import java.io.File;

import javax.net.ssl.SSLContext;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;

public class HttpClient {
	

	
private HttpClient() {
		super();
		// TODO Auto-generated constructor stub
	}

public static CloseableHttpClient getHttpClient(String url) {
		
		// set timeout with 10 seconds.
		int connectTimeout=10 * 1000;
		int socketTimeout=20 * 1000;
	    RequestConfig config = RequestConfig.custom().setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
	  
		CloseableHttpClient httpClient= HttpClients.custom()
										.setDefaultRequestConfig(config)
										.build();
	    /*
	     * This is for retry 
	     * .setRetryHandler(CustomRetryHandler.process()) 
	     * 
	     * */
		try {
			if(!url.toLowerCase().startsWith("https")) {
				return httpClient;
			}else{
				String trustKeystoreFile=System.getProperty("java.home")
						+System.getProperty("file.separator")
						+"lib"+System.getProperty("file.separator")
						+"security"+System.getProperty("file.separator")
						+"cacerts";
				LoggingUtil.logDebug("TrustKeystoreFile::> "+ trustKeystoreFile);
				char[] sslStoreClient = "changeit".toCharArray();
				
				 SSLContext sslcontext = SSLContexts.custom()
			                .loadTrustMaterial(new File(trustKeystoreFile), sslStoreClient,
			                        new TrustSelfSignedStrategy())
			                .build();
				 SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
			                sslcontext,
			                new String[] { "TLSv1.2" },
			                null,
			                SSLConnectionSocketFactory.getDefaultHostnameVerifier());
				 httpClient = HttpClients.custom()
						 	.setDefaultRequestConfig(config)
			                .setSSLSocketFactory(sslsf)
			                .build();
			}
		}catch(Exception e) {
			LoggingUtil.logError(e);
		}
		return httpClient;
	}

}
