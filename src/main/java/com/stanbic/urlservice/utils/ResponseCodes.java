package com.stanbic.urlservice.utils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class ResponseCodes  {
	public static final String SUCCESS = "000";
	public static final String INTERFACE_ERROR = "A01";
	public static final String DUPLICATE_REQUEST = "A02";
	public static final String RECORD_NOT_FOUND = "A03";
	public static final String INVALID_REQUEST_TOKEN = "A04";
	public static final String FAILED = "A05";
	public static final String INVALID_INPUT = "A06";
	public static final String NOT_SUCCESS = "A07";
	public static final String SOURCE_CODE_NOT_FOUND = "A08";
	
	private static Map<String, ResourceBundle> resourceMap = new HashMap<>();

	
	
	private ResponseCodes() {
		super();
		// TODO Auto-generated constructor stub
	}


	public static String getMessage(String errorCode)
	{
		return getMessage(errorCode, null);
	}
	

	public static String getMessage(String errorCode, String langCode)
	{
		if(langCode == null)
			langCode = Locale.ENGLISH.getLanguage();

		ResourceBundle resource = resourceMap.get(langCode);
		if(resource == null )
		{
			resource = ResourceBundle.getBundle("ResponseMessages", new Locale(langCode));
			resourceMap.put(langCode, resource);
		}

		String[] errors = errorCode.split("-");
		return resource.getString(errors[0].trim()) + ((errors.length > 1 && !errors[1].trim().isEmpty()) ? (" - " + errors[1].trim()) : "");
	}
}
