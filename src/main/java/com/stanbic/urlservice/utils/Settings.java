package com.stanbic.urlservice.utils;

import java.io.InputStream;
import java.util.Properties;

public class Settings {
	
    private static Settings uniqueInstance = null;
    private Properties props;
 //	This value is A JVM Property	
    private static String configFileName = "AppProps-UAT.properties";
    
    private Settings() {
        props = new Properties();
        loadPropertyFile();
    }
    
    private synchronized void loadPropertyFile() {
      try 
      {
    	  props.load(Settings.class.getClassLoader().getResourceAsStream(configFileName));
      }
      catch(Exception ex)
      {
    	 LoggingUtil.logError(ex);
      }
   	
    }
   
    public static synchronized Settings getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new Settings();
        }
        return uniqueInstance;
    }

    public synchronized String getProperty(String s) {
        return props.getProperty(s);
    }

    public static String[] tokenize(String input, String delim) {
       return input.split(delim);
    }
    
    public static InputStream resourceFileReader(String file) {
    	return  Settings.class.getClassLoader().getResourceAsStream(file);
    }
    
}
