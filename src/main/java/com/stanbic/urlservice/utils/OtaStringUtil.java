/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stanbic.urlservice.utils;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;


public class OtaStringUtil {

    private static final int PAD_LIMIT = 8192;

    private static SimpleDateFormat dff = new SimpleDateFormat("MM-DD-yy hhmm a");
    private static SimpleDateFormat dff2 = new SimpleDateFormat("DDMMyyyy");
    private static SimpleDateFormat flexSdf = new SimpleDateFormat("yyyyMMdd");
    
    private static SimpleDateFormat dff3 = new SimpleDateFormat("dd/MM/yyyy");
    
    private static SimpleDateFormat dff4 = new SimpleDateFormat("dd-MMM-yyyy");
    private static SimpleDateFormat dff4_1 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
    private static SimpleDateFormat dff5 = new SimpleDateFormat("dd-MM-yyyy");
    private static SimpleDateFormat dff6 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
    
    private static SimpleDateFormat dffTZ = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
    
    private static SimpleDateFormat dffExp = new SimpleDateFormat("MMyyyy");
    
    private static SimpleDateFormat dffExp2 = new SimpleDateFormat("dd-MM-yy");
   
    
    
    private OtaStringUtil() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static String getEasternDateFormat(Date dt)
    {
      return dff.format(dt) + " EDT";
    }
    
    public static Date convertToDateS(String dt)
    {
       Date dtx = null;
       try 
       { 
    	   dtx = dff5.parse(dt);
       }
       catch(Exception ex)
       {
    	  LoggingUtil.logError(ex);
       }
       
       return dtx;
    }
    
    public static Date convertoDateExpiryMMYY(String dt)
    {
    	 
    	try {
  		     //dt = dt.replace("T", "").trim();
	  	     Date d = dffExp2.parse(dt.trim());
	  	     return d;
	    }
	    catch(Exception ex)
	    {
	  	  LoggingUtil.logError(ex);
	    }
    	
    	return new Date();
    }
    
    public static String convertoDateMMYYYY(Date dt)
    {
    	 
         return dffExp.format(dt);
    }
    
    public static String convertoDate(Date dt)
    {
    	 
         return dff5.format(dt);
    }
    
    public static String convertoFlexmlDate(Date dt)
    {
    	 
         return flexSdf.format(dt);
    }
    
    public static String getOracleDateString(Date dt)
    {
    	 
         return dff4.format(dt).toUpperCase();
    }

    public static String getDateFormat2(Date dt)
    {
      return dff2.format(dt) ;
    }
    
    public static String getDateFormat3(Date dt)
    {
      return dff3.format(dt) ;
    }
    
    public static String convertDateToStringOracle(String dt)
    {
      String st = "";
    	try {
    	Date d = dff3.parse(dt.trim());
    	st = dff4.format(d) ;
      }
      catch(Exception ex)
      {
    	  LoggingUtil.logError(ex);
    	  LoggingUtil.logError(ex);
      }
    	return st;
    }
    
    public static Date convertToOracleDate(String dt)
    {
    	Date d=null;
    	try {
			 d=dff4.parse(dt);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			 LoggingUtil.logError(e);
		}
    	return d;
    }
    
    public static Date convertToOracleDateTime(String dt)
    {
    	Date d=null;
    	try {
			 d=dff4_1.parse(dt);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			 LoggingUtil.logError(e);
		}
    	return d;
    }
    
    public static String getTodayString()
    {
    	String today="";
    	today=dff4_1.format(new Date());
    	return today;
    }
    
    public static String getTodayDateString()
    {
    	String today="";
    	today=dff4.format(new Date());
    	return today;
    }
    
    public static String getTodayStringDDMMMYYYY()
    {
    	String today="";
    	today=dff4.format(new Date());
    	return today;
    }
    
    public static Date tzStringToDate(String dt)
    {
      //String st = "";
    	try {
    		  //dt = dt.replace("T", "").trim();
    	Date d = dffTZ.parse(dt.trim());
    	return d;
      }
      catch(Exception ex)
      {
    	  LoggingUtil.logError(ex);
      }
    	return new Date();
    }
    
	public static boolean isLeapYear(int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		return cal.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;
	}
  
	public static int getLastDay(int mth, int yr) {
		boolean isleap = isLeapYear(yr);
		if (mth == 4 || mth == 6 || mth == 9 || mth == 11)
			return 30;
		else if (mth == 2) {
			if (isleap)
				return 29;
			else
				return 28;
		} else {
			return 31;
		}
	}
  
	public static int[] tetWeekDays(int d) {
		int[] days = new int[2];
		days[0] = 0;
		days[1] = 0;

		if (d == Calendar.MONDAY) {
			days[0] = 0;
			days[1] = 0;
		} else if (d == Calendar.TUESDAY) {
			days[0] = -1;
			days[1] = 0;
		} else if (d == Calendar.WEDNESDAY) {
			days[0] = -2;
			days[1] = 0;
		} else if (d == Calendar.THURSDAY) {
			days[0] = -3;
			days[1] = 0;
		} else if (d == Calendar.FRIDAY) {
			days[0] = -4;
			days[1] = 0;
		} else if (d == Calendar.SATURDAY) {
			days[0] = -5;
			days[1] = 0;
		} else if (d == Calendar.SUNDAY) {
			days[0] = -6;
			days[1] = 0;
		}

		return days;

	}
	
	public static Date addDaysToDate(Date dt,int days)
    {
    	Calendar c = Calendar.getInstance();
    	c.setTime(dt); // Now use today date.
    	c.add(Calendar.DATE, days); 
    	return  c.getTime();
    }
	
	public static Date[]  getDateRange(String periodType, Date dt)
	{
		Date[] dts = new Date[]{dt,dt};
		
		try
		{
			String dt1 = "";
			Calendar cal = Calendar.getInstance();
			
			if(periodType.equals("D"))
				return dts;
			else if(periodType.equals("W"))
			{
				cal.setTime(dt);
				if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY )
				{
					dts[0] = dt;
					dts[1] = dt;
				}
				else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY )
				{
					dts[0] = addDaysToDate(dt, -1);
					dts[1] = dt;
				}
				else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY )
				{
					dts[0] = addDaysToDate(dt, -2);
					dts[1] = dt;
				}
				else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY )
				{
					dts[0] = addDaysToDate(dt, -3);
					dts[1] = dt;
				}
				else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY )
				{
					dts[0] = addDaysToDate(dt, -4);
					dts[1] = dt;
				}
				else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY )
				{
					dts[0] = addDaysToDate(dt, -5);
					dts[1] = dt;
				}
				else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY )
				{
					dts[0] = addDaysToDate(dt, -6);
					dts[1] = dt;
				}
				else if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY )
				{
					dts[0] = addDaysToDate(dt, -7);
					dts[1] = dt;
				}
			}
			else if(periodType.equals("M"))
			{
				cal.setTime(dt);
				int m = cal.get(Calendar.MONTH) + 1;
				int yr = cal.get(Calendar.YEAR);
				int lday = getLastDay(m, yr);
				if(m < 10)
					dt1 = "01-0" + m + "-" + yr;
				else
					dt1 = "01-" + m + "-" + yr;
				
				dts[0] = convertToDateS(dt1);
				dt1 = lday + "-" + m + "-" + yr;
				dts[1] = convertToDateS(dt1);
				
			}
			else if(periodType.equals("Y"))
			{
				cal.setTime(dt);
				//int m = cal.get(Calendar.MONTH) + 1;
				int yr = cal.get(Calendar.YEAR);
				int lday = getLastDay(12, yr);
				dt1 = "01-01-" + yr;
				dts[0] = convertToDateS(dt1);
				dt1 = lday + "-" + "12" + "-" + yr;
				dts[1] = convertToDateS(dt1);
				
			}
			
			
		}
		catch(Exception ex)
		{
			LoggingUtil.logError(ex);
		}
		
		return dts;
	}

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static String leftPad(String str, int size, char padChar) {
        if (str == null) {
            return null;
        }
        int pads = size - str.length();
        if (pads <= 0) {
            return str; // returns original String when possible
        }
        if (pads > PAD_LIMIT) {
            return leftPad(str, size, String.valueOf(padChar));
        }
        return padding(pads, padChar).concat(str);
    }

    public static String leftPad(String str, int size, String padStr) {
        if (str == null) {
            return null;
        }
        if (isEmpty(padStr)) {
            padStr = " ";
        }
        int padLen = padStr.length();
        int strLen = str.length();
        int pads = size - strLen;
        if (pads <= 0) {
            return str; // returns original String when possible
        }
        if (padLen == 1 && pads <= PAD_LIMIT) {
            return leftPad(str, size, padStr.charAt(0));
        }

        if (pads == padLen) {
            return padStr.concat(str);
        } else if (pads < padLen) {
            return padStr.substring(0, pads).concat(str);
        } else {
            char[] padding = new char[pads];
            char[] padChars = padStr.toCharArray();
            for (int i = 0; i < pads; i++) {
                padding[i] = padChars[i % padLen];
            }
            return new String(padding).concat(str);
        }
    }

    public static String rightPad(String str, int size, char padChar) {
        if (str == null) {
            return null;
        }
        int pads = size - str.length();
        if (pads <= 0) {
            return str; // returns original String when possible
        }
        if (pads > PAD_LIMIT) {
            return rightPad(str, size, String.valueOf(padChar));
        }
        return str.concat(padding(pads, padChar));
    }

    public static String rightPad(String str, int size, String padStr) {
        if (str == null) {
            return null;
        }
        if (isEmpty(padStr)) {
            padStr = " ";
        }
        int padLen = padStr.length();
        int strLen = str.length();
        int pads = size - strLen;
        if (pads <= 0) {
            return str; // returns original String when possible
        }
        if (padLen == 1 && pads <= PAD_LIMIT) {
            return rightPad(str, size, padStr.charAt(0));
        }

        if (pads == padLen) {
            return str.concat(padStr);
        } else if (pads < padLen) {
            return str.concat(padStr.substring(0, pads));
        } else {
            char[] padding = new char[pads];
            char[] padChars = padStr.toCharArray();
            for (int i = 0; i < pads; i++) {
                padding[i] = padChars[i % padLen];
            }
            return str.concat(new String(padding));
        }
    }

    private static String padding(int repeat, char padChar) throws IndexOutOfBoundsException {
        if (repeat < 0) {
            throw new IndexOutOfBoundsException("Cannot pad a negative amount: " + repeat);
        }
        final char[] buf = new char[repeat];
        for (int i = 0; i < buf.length; i++) {
            buf[i] = padChar;
        }
        return new String(buf);
    }

    public static String getReferenceNo(String type) throws NoSuchAlgorithmException {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        char[] stringChars = new char[7];
        int x = 0;
        Random random = SecureRandom.getInstanceStrong();
        for (int i = 0; i < 3; i++) //3
        {
            x = random.nextInt(chars.length());
            stringChars[i] = chars.charAt(x);
        }
        SimpleDateFormat isoSdf = new SimpleDateFormat("mmss");
        Date dt = new Date();
        for (int i = 3; i < 7; i++) //4
        {
            x = random.nextInt(9);
            stringChars[i] = chars.charAt(x);
        }


        String finalString = new String(stringChars) + isoSdf.format(dt);
       return type + finalString;
    }

    public static String getRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }

    public static String getISOAmount(double amt) {
        String amtx = "";
        amtx = BigDecimal.valueOf(amt ).setScale(0, RoundingMode.HALF_UP).toString();
        int x = amtx.indexOf(".");
        if (x > 0) {
            amtx = amtx.substring(0, x);
        }
        try {
            //amtx = ISOUtil.padleft(amtx, 12, '0');
        } catch (Exception ex) {
        }

        return amtx;
    }
    
    public static String generateRandomNumber(int charLength) {
        return String.valueOf(charLength < 1 ? 0 : new Random()
                .nextInt((9 * (int) Math.pow(10, charLength - 1)) - 1)
                + (int) Math.pow(10, charLength - 1));
    }

       public static String getRetrievalRefNo(String type) {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        char[] stringChars = new char[7];

        int x = 0; //stringChars.length;
        for (int i = 0; i < 3; i++) //3
        {
            Random random = new Random();
            x = random.nextInt(chars.length());
            stringChars[i] = chars.charAt(x);
        }
        SimpleDateFormat isoSdf = new SimpleDateFormat("mmss");
        Date dt = new Date();

        for (int i = 3; i < 7; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);
            stringChars[i] = chars.charAt(x);
        }


        String finalString = new String(stringChars) + isoSdf.format(dt);
        String refno = type + finalString;

        return refno;
    }

       public static String formatAmount(double d)
    {
          DecimalFormat df = new DecimalFormat("#,###.##");
          return df.format(d);
       }

    public static String getStan() {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[6];
        for (int i = 0; i < 6; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        return finalString.trim();
    }
    
    public static XMLGregorianCalendar getXMLGregDate(Date d) throws Exception
    { 
    	GregorianCalendar gc = new GregorianCalendar();
    
           gc.setTimeInMillis(d.getTime());
       return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
    }
    
    
    public static XMLGregorianCalendar getXMLGregDate2(Date d) throws Exception
    { 
    	GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(d);
		
		XMLGregorianCalendar xmlDate =  DatatypeFactory.newInstance().newXMLGregorianCalendar(
			        cal.get(Calendar.YEAR),
			        cal.get(Calendar.MONTH) + 1,
			        cal.get(Calendar.DAY_OF_MONTH),
			        cal.get(Calendar.HOUR_OF_DAY),
			        cal.get(Calendar.MINUTE),
			        cal.get(Calendar.SECOND), 
			        DatatypeConstants.FIELD_UNDEFINED,
			        DatatypeConstants.FIELD_UNDEFINED);
		
		return xmlDate;
    }
 
    public static int dateDifference(String dateBeforeStr,String dateAfterStr) 
    { 
    	int days=0;
    	 try {
	  	       Date dateBefore = dff4.parse(dateBeforeStr);
	  	       Date dateAfter = dff4.parse(dateAfterStr);
	  	       long difference = dateAfter.getTime() - dateBefore.getTime();
	  	        days =(int) (difference / (1000*60*60*24));
		  	 } catch (Exception e) {
		  		 LoggingUtil.logError(e);
		  	 }
		return days;
    }
    
    public static String currentDate()
    { 
    	String cDate="";
    	 try {
	  	       cDate= dff4.format(new Date());
		  	 } catch (Exception e) {
		  	       LoggingUtil.logError(e);
		  	 }
		return cDate;
    }
    
    public static String convertDDMMYYYY(String inputString) {
    	String result="";
    	 try {
    		 if(inputString != null && !inputString.equalsIgnoreCase("")) {
    			 Date date = dff6.parse(inputString);
        		 result=dff3.format(date);     
    		 }		 
	  	 } catch (Exception e) {
	  	       LoggingUtil.logError(e);
	  	 }
		return result;
    }
    
    public static String dateForPreAgreement(String inputString) {
    	String result="";
    	 try {
    		 if(inputString != null && !inputString.equalsIgnoreCase("")) {
    			 Date date = dff4.parse(inputString);
        		 result=dff3.format(date);     
    		 }		 
	  	 } catch (Exception e) {
	  	       LoggingUtil.logError(e);
	  	 }
		return result;
    }
    
    
    public static String convertDDMMMYYYY(String inputString) {
    	String result="";
    	 try {
    		 if(inputString != null && !inputString.equalsIgnoreCase("")) {
    			 Date date = dff6.parse(inputString);
        		 result=dff4.format(date);     
    		 }		 
	  	 } catch (Exception e) {
	  	       LoggingUtil.logError(e);
	  	 }
		return result;
    }
    
    public static String convertDDMMMYYYYTIME(String inputString) {
    	String result="";
    	 try {
    		 if(inputString != null && !inputString.equalsIgnoreCase("")) {
    			 Date date = dff6.parse(inputString);
        		 result=dff4_1.format(date);     
    		 }		 
	  	 } catch (Exception e) {
	  	       LoggingUtil.logError(e);
	  	 }
		return result;
    }
    
    public static String formatPercent(double done, int digits) {
        DecimalFormat percentFormat = new DecimalFormat("0.0%");
        percentFormat.setDecimalSeparatorAlwaysShown(false);
        percentFormat.setMinimumFractionDigits(digits);
        percentFormat.setMaximumFractionDigits(digits);
        return percentFormat.format(done);
    }

}
