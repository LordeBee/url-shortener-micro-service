package com.stanbic.urlservice.funtions;

import java.util.concurrent.ExecutorService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.stanbic.urlservice.utils.LoggingUtil;

@WebListener
public class ApplicationStartUpListener implements ServletContextListener {
	public static ExecutorService executor= null;
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		LoggingUtil.logDebug("App Service Started");
		
		// Uncomment below if needed
		/*if(executor == null) {
			executor= Executors.newFixedThreadPool(2);
		}*/
		
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		LoggingUtil.logDebug("App Service Destroyed");
		if(executor != null) {
			executor.shutdownNow();
		}
	}
	

}
