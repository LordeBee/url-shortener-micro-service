package com.stanbic.urlservice.funtions;


import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.ws.rs.core.MediaType;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.stanbic.urlservice.restmodels.UrlInfo;
import com.stanbic.urlservice.restmodels.UrlRequest;
import com.stanbic.urlservice.restmodels.UrlResponse;
import com.stanbic.urlservice.utils.OtaStringUtil;
import com.stanbic.urlservice.utils.Settings;
import com.stanbic.urlservice.utils.HttpClient;

@Service
public class UrlShortener {
	
	public OtaStringUtil randNumber;

	static final String URL_SHORTEN_URL=Settings.getInstance().getProperty("URL_SHORTEN_URL");
    static final Gson g=new Gson();
    
    
    public String generateUrl(String destination) {
    	String url = null;
    	UrlRequest req = new UrlRequest();
    	UrlInfo resp = new UrlInfo();
    	String slashtag1 = OtaStringUtil.getRefNumber("",6);
    	req.setSlashtag("sbg"+slashtag1);
    	req.setRedirectCode("301");
    	req.setDestination(destination);
    	
    	String reqBody= g.toJson(req);

    	try(CloseableHttpClient c = HttpClient.getHttpClient(URL_SHORTEN_URL)){
    		
    		HttpPost post=new HttpPost(URL_SHORTEN_URL);
    		post.setHeader("Content-Type", MediaType.APPLICATION_JSON);
		    post.addHeader("ACCEPT",MediaType.APPLICATION_JSON);
		    post.setHeader("apikey", "6zsxNFyik9XW1dCEtR6s");
		    post.setHeader("apisecret", "pq2SYWbCeeEN");
		    HttpEntity entity = new ByteArrayEntity(reqBody.getBytes(StandardCharsets.UTF_8));
		    post.setEntity(entity);
		    HttpResponse response = c.execute(post);
		    int statusCode=response.getStatusLine().getStatusCode();
		    if(statusCode == 200) {
		    	 String respStr=EntityUtils.toString(response.getEntity());
		    	UrlResponse respBody = g.fromJson(respStr, UrlResponse.class);
		    	if(respBody.getStatus().equals(200)) {
		    		url = respBody.getData().getShortUrl();
		    	}
		    }	 
    		
    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return url;
    	
    	
    }
}
