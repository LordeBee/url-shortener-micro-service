package com.stanbic.urlservice.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.stanbic.urlservice.restmodels.UrlUserResponse;
import com.stanbic.urlservice.utils.ResponseCodes;
import com.stanbic.urlservice.utils.LoggingUtil;
import com.stanbic.urlservice.restmodels.HeaderResponse;
import com.stanbic.urlservice.restmodels.UrlRequest;
import com.stanbic.urlservice.restmodels.UrlUserRequest;
import com.stanbic.urlservice.restmodels.HostHeaderInfo;
import com.stanbic.urlservice.restmodels.LinkInfo;
import 	com.stanbic.urlservice.funtions.UrlShortener;
import com.stanbic.urlservice.restmodels.UrlInfo;


@RestController
@RequestMapping("/api/v1")
public class AppController {
	
	Gson g= new Gson();
	
	@Autowired
	UrlShortener urlShortener;

	
	@GetMapping("/echotest")
	public String webServiceEcho() 
	{
		return "TestApp Echotest ".toUpperCase();
	}
	
	@PostMapping(value = "/generateUrl", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public UrlUserResponse UrlShortener(HttpServletRequest request,HttpServletResponse response,@RequestBody UrlUserRequest req) {
		
		HostHeaderInfo hdr=new HostHeaderInfo();
		UrlUserResponse resp = new UrlUserResponse();
		UrlInfo respData = new UrlInfo();
		System.out.println("url req:: "+ g.toJson(req));
		try {
			LinkInfo linkreq = req.getLinkInfo();
			String url = urlShortener.generateUrl(linkreq.getUrl());
			System.out.println("Converted url:" + url);
			
			
			if(url != null) {
				respData.setUrl(url);
				resp.setUrlInfo(respData);
				hdr.setResponseCode(ResponseCodes.SUCCESS);
				hdr.setResponseMessage(ResponseCodes.getMessage(ResponseCodes.SUCCESS));
			}
			else {
				
				hdr.setResponseCode(ResponseCodes.FAILED);
				hdr.setResponseMessage(ResponseCodes.getMessage(ResponseCodes.FAILED));
				
			}
			
		}catch (Exception ex) {
			LoggingUtil.logError(ex);
			
			hdr.setResponseCode(ResponseCodes.INTERFACE_ERROR);
			hdr.setResponseMessage(ResponseCodes.getMessage(ResponseCodes.INTERFACE_ERROR));
		} 	
		resp.setHostHeaderInfo(hdr);
		return resp;
		
	}

}
