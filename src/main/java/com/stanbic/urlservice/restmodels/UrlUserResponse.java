
package com.stanbic.urlservice.restmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UrlUserResponse {

    @SerializedName("hostHeaderInfo")
    @Expose
    private HostHeaderInfo hostHeaderInfo;
    @SerializedName("urlInfo")
    @Expose
    private UrlInfo urlInfo;

    public HostHeaderInfo getHostHeaderInfo() {
        return hostHeaderInfo;
    }

    public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
        this.hostHeaderInfo = hostHeaderInfo;
    }

    public UrlInfo getUrlInfo() {
        return urlInfo;
    }

    public void setUrlInfo(UrlInfo urlInfo) {
        this.urlInfo = urlInfo;
    }

}
