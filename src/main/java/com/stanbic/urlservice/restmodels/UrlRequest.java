
package com.stanbic.urlservice.restmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UrlRequest {

    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("slashtag")
    @Expose
    private String slashtag;
    @SerializedName("redirectCode")
    @Expose
    private String redirectCode;

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getSlashtag() {
        return slashtag;
    }

    public void setSlashtag(String slashtag) {
        this.slashtag = slashtag;
    }

    public String getRedirectCode() {
        return redirectCode;
    }

    public void setRedirectCode(String redirectCode) {
        this.redirectCode = redirectCode;
    }

}
