
package com.stanbic.urlservice.restmodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.stanbic.urlservice.restmodels.HeaderRequest;

public class UrlUserRequest {

    @SerializedName("hostHeaderInfo")
    @Expose
    private com.stanbic.urlservice.restmodels.HeaderRequest hostHeaderInfo;
    @SerializedName("linkInfo")
    @Expose
    private LinkInfo linkInfo;

    public HeaderRequest getHostHeaderInfo() {
        return hostHeaderInfo;
    }

    public void setHostHeaderInfo(HeaderRequest value) {
        this.hostHeaderInfo = value;
    }

    public LinkInfo getLinkInfo() {
        return linkInfo;
    }

    public void setLinkInfo(LinkInfo linkInfo) {
        this.linkInfo = linkInfo;
    }

}
